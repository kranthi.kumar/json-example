class Rectanle:
    def __init__(self, length, width):
        self.length = length
        self.width = width

    def area(self):
        return self.length * self.width

    def perimeter(self):
        return 2 * (self.length + self.width)


class Cricle:
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return 3.14 * self.radius * self.radius

    def perimeter(self):
        return 2 * 3.14 * self.radius


class Square(Rectanle):
    def __init__(self, side):
        super().__init__(side, side)


class Shape:
    @staticmethod
    def tocalculate(shap):
        if shap == 'Rectangle':
            length = int(input("Please enter the length of Rectangle : "))
            width = int(input("Please enter the length of Rectangle : "))
            rectangle_object = Rectanle(length, width)
            a = rectangle_object.area()
            p = rectangle_object.perimeter()
            d = (a, p)
            return d
        elif shap == 'Circle':
            radius = int(input("Please enter the Radius of Circle : "))
            circle_object = Cricle(radius)
            a = circle_object.area()
            p = circle_object.perimeter()
            d = (a, p)
            return d
        elif shap == 'Square':
            side = int(input("Please enter the side of Square : "))
            square_object = Square(side)
            a = square_object.area()
            p = square_object.perimeter()
            d = (a, p)
            return d


Shap = input("Please enter the Shape Your are Looking For:")
result = Shape().tocalculate(Shap)
print("Area of ", Shap, " is :", result[0])
print("perimeter of ", Shap, " is :", result[1])
