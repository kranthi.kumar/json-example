class Student:
    @staticmethod
    def data(name, Class, college, department, courses):
        dictionary = {"Name": name, "Class": Class, "College": college, "Department": department, "Courses": courses}
        return dictionary
