import json


class Convert:
    def __init__(self, dictionary):
        self.dictionary = dictionary

    def convert(self):
        jsonStr = json.dumps(self.dictionary)
        return jsonStr
